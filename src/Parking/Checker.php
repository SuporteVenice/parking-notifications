<?php

namespace ParkingNotifications\Parking;

class Checker {

	public $connection;

	public $notificationCreator;

	public function __construct($connection, $notificationCreator) {

		$this->connection = $connection;

		$this->notificationCreator = $notificationCreator;

	}

	public function remainingBalance($entityId) {

		if(!is_numeric($entityId))
			throw new \InvalidArgumentException("Numeric required");

		$entityId = (int) $entityId;

		$query = "SELECT id, SUM(pre_creditado + pre_utilizado + pre_creditado_estorno + pre_utilizado_estorno) as balance
					FROM limite
					WHERE entidade_id = ?
					AND bolsa_id = 1
					LIMIT 1";

		$stmt = $this->connection->prepare($query);
		$stmt->bindValue(1, $entityId);
		$stmt->execute();

		$limit = $stmt->fetch();

		$query = "SELECT valor as value
					FROM configuracao
					WHERE chave = 'AVISO_CLIENTE_SALDO_MINIMO'";

		$stmt = $this->connection->prepare($query);
		$stmt->execute();

		$configuration = $stmt->fetch();

		if ($limit['balance'] <= $configuration['value'])
			$this->notificationCreator->remainingBalance($entityId);

	}

	public function rechargeSuccess($entityId, $value) {

		if(!is_numeric($entityId))
			throw new \InvalidArgumentException("Numeric required");

		$entityId = (int) $entityId;

		$this->notificationCreator->rechargeSuccess($entityId, $value);

	}

}

?>
