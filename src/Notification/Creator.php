<?php

namespace ParkingNotifications\Notification;

use ParkingNotifications\Notification\Requester;

class Creator {

	public $firebaseRequester;

	public function requester($connection, $origin) {

		return new Requester($connection, $origin);

	}

	public function __construct($connection, $origin) {

		$this->firebaseRequester = $this->requester($connection, $origin);

	}

	public function remainingBalance($entityId) {

		$title = 'REMAINING_BALANCE_TITLE';

		$message = 'REMAINING_BALANCE_MESSAGE';

		$tag = 'remaining_balance';

		$this->firebaseRequester->sendByEntityId($entityId, $title, $message, $tag);

	}

	public function irregularityNotice($plate, $vehicleType, $type) {

		$title = 'IRREGULARITY_NOTICE_TITLE';

		$message = 'IRREGULARITY_NOTICE_MESSAGE_' . $type;

		$tag = 'irregularity_notice';

		$translatorParams = [
			'plate' => $plate
		];

		$this->firebaseRequester->sendByPlateAndVehicleType($plate, $vehicleType, $title, $message, $tag, $translatorParams);

	}

	public function rechargeSuccess($entityId, $value) {

		$title = 'RECHARGE_SUCCESS_TITLE';

		$message = 'RECHARGE_SUCCESS_MESSAGE';

		$tag = 'recharge_success';

		$translatorParams = [
			'value' => $value
		];

		$this->firebaseRequester->sendByEntityId($entityId, $title, $message, $tag, $translatorParams);

	}

	public function ticketActivated($plate, $vehicleType, $endTime, $value) {

		$title = 'TICKET_ACTIVATED_TITLE';

		$message = 'TICKET_ACTIVATED_MESSAGE';

		$tag = 'ticket_activated';

		$translatorParams = [
			'plate' => $plate,
			'endTime' => $endTime,
			'value' => $value
		];

		$this->firebaseRequester->sendByPlateAndVehicleType($plate, $vehicleType, $title, $message, $tag, $translatorParams);

	}

	public function ticketDeactivated($plate, $vehicleType, $value) {

		$title = 'TICKET_DEACTIVATED_TITLE';

		$message = 'TICKET_DEACTIVATED_MESSAGE';

		$tag = 'ticket_deactivated';

		$translatorParams = [
			'plate' => $plate,
			'value' => $value
		];

		$this->firebaseRequester->sendByPlateAndVehicleType($plate, $vehicleType, $title, $message, $tag, $translatorParams);

	}

	public function ticketCanceled($plate, $vehicleType, $value) {

		$title = 'TICKET_CANCELED_TITLE';

		$message = 'TICKET_CANCELED_MESSAGE';

		$tag = 'ticket_canceled';

		$translatorParams = [
			'plate' => $plate,
			'value' => $value
		];

		$this->firebaseRequester->sendByPlateAndVehicleType($plate, $vehicleType, $title, $message, $tag, $translatorParams);

	}

	public function ticketEnding($plate, $vehicleType, $endTime) {

		$title = 'TICKET_ENDING_TITLE';

		$message = 'TICKET_ENDING_MESSAGE';

		$tag = 'ticket_ending';

		$translatorParams = [
			'plate' => $plate,
			'endTime' => $endTime
		];

		$this->firebaseRequester->sendByPlateAndVehicleType($plate, $vehicleType, $title, $message, $tag, $translatorParams);

	}

	public function plateExceeding($plate, $vehicleType, $exceedTime) {

		$title = 'PLATE_EXCEEDING_TITLE';

		$message = 'PLATE_EXCEEDING_MESSAGE';

		$tag = 'plate_exceeding';

		$translatorParams = [
			'plate' => $plate,
			'exceedTime' => $exceedTime
		];

		$this->firebaseRequester->sendByPlateAndVehicleType($plate, $vehicleType, $title, $message, $tag, $translatorParams);

	}

}

?>
