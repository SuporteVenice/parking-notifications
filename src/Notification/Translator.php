<?php

namespace ParkingNotifications\Notification;

class Translator {

	public $language;

	private $strings = [
		'pt-BR' => [
			'REMAINING_BALANCE_TITLE' => 'Seu saldo está terminando',
			'REMAINING_BALANCE_MESSAGE' => 'Faça uma recarga através do aplicativo!',

			'IRREGULARITY_NOTICE_TITLE' => 'Você recebeu uma irregularidade na placa {{plate}}',
			'IRREGULARITY_NOTICE_MESSAGE_NENHUM' => 'O seu veículo de placa {{plate}} recebeu uma irregularidade. Por favor, regularize sua situação.',
			'IRREGULARITY_NOTICE_MESSAGE_VENCIDO' => 'O seu veículo de placa {{plate}} recebeu uma irregularidade por estar com o ticket vencido. Por favor, regularize sua situação.',
			'IRREGULARITY_NOTICE_MESSAGE_FORA_DA_VAGA' => 'O seu veículo de placa {{plate}} recebeu uma irregularidade por estar fora da vaga. Por favor, regularize sua situação.',
			'IRREGULARITY_NOTICE_MESSAGE_TICKET_INCOMPATIVEL' => 'O seu veículo de placa {{plate}} recebeu uma irregularidade por estar com o ticket incompatível. Por favor, regularize sua situação.',
			'IRREGULARITY_NOTICE_MESSAGE_SEM_TICKET' => 'O seu veículo de placa {{plate}} recebeu uma irregularidade por estar sem ticket. Por favor, regularize sua situação.',
			'IRREGULARITY_NOTICE_MESSAGE_PERMANENCIA_EXCEDIDA' => 'O seu veículo de placa {{plate}} recebeu uma irregularidade por permanência excedida. Por favor, regularize sua situação.',
			'IRREGULARITY_NOTICE_MESSAGE_SEM_CREDENCIAL' => 'O seu veículo de placa {{plate}} recebeu uma irregularidade por estar sem credencial. Por favor, regularize sua situação.',
			'IRREGULARITY_NOTICE_MESSAGE_TICKET_EXTERNO_SEM_CARTAO' => 'O seu veículo de placa {{plate}} recebeu uma irregularidade pelo motivo de ticket externo sem cartão. Por favor, regularize sua situação.',
			'IRREGULARITY_NOTICE_MESSAGE_TICKET_EXTERNO_CARTAO_IRREGULAR' => 'O seu veículo de placa {{plate}} recebeu uma irregularidade pelo motivo de ticket externo com cartão irregular. Por favor, regularize sua situação.',
			'IRREGULARITY_NOTICE_MESSAGE_TICKET_EXTERNO_CARTAO_NAO_LEGIVEL' => 'O seu veículo de placa {{plate}} recebeu uma irregularidade pelo motivo de ticket externo com cartão ilegível. Por favor, regularize sua situação.',
			'IRREGULARITY_NOTICE_MESSAGE_TICKET_EXTERNO_CARTAO_HORARIO_VENCIDO' => 'O seu veículo de placa {{plate}} recebeu uma irregularidade pelo motivo de ticket externo com horário vencido. Por favor, regularize sua situação.',

			'RECHARGE_SUCCESS_TITLE' => 'Sua recarga foi aprovada',
			'RECHARGE_SUCCESS_MESSAGE' => 'Uma recarga de R${{value}} foi aprovada na sua conta.',

			'TICKET_ACTIVATED_TITLE' => 'Ticket ativado',
			'TICKET_ACTIVATED_MESSAGE' => 'Um ticket de R${{value}}, válido até {{endTime}} foi ativado para a placa {{plate}}.',

			'TICKET_DEACTIVATED_TITLE' => 'Ticket desativado',
			'TICKET_DEACTIVATED_MESSAGE' => 'Um ticket de R${{value}} foi desativado para a placa {{plate}}.',

			'TICKET_CANCELED_TITLE' => 'Ticket cancelado',
			'TICKET_CANCELED_MESSAGE' => 'Um ticket de R${{value}} foi cancelado para a placa {{plate}}.',

			'TICKET_ENDING_TITLE' => 'Lembrete de término de ticket',
			'TICKET_ENDING_MESSAGE' => 'O ticket para a placa {{plate}} irá expirar às {{endTime}}.',

			'PLATE_EXCEEDING_TITLE' => 'Lembrete de tempo excedido',
			'PLATE_EXCEEDING_MESSAGE' => 'O veículo de placa {{plate}} excederá o tempo máximo de permanência na vaga às {{exceedTime}}. Por favor, troque o mesmo de vaga e evite irregularidades.',
		],
		'en-US' => [
			'REMAINING_BALANCE_TITLE' => 'Your balance is finishing',
			'REMAINING_BALANCE_MESSAGE' => 'Recharge through the app!',

			'IRREGULARITY_NOTICE_TITLE' => 'You received a new irregularity on {{plate}}',
			'IRREGULARITY_NOTICE_MESSAGE_NENHUM' => 'Your vehicle of plate {{plate}} received a new irregularity. Please, regularize your account.',
			'IRREGULARITY_NOTICE_MESSAGE_VENCIDO' => 'Your vehicle of plate {{plate}} received a new irregularity because ticket has expired. Please, regularize your account.',
			'IRREGULARITY_NOTICE_MESSAGE_FORA_DA_VAGA' => 'Your vehicle of plate {{plate}} received a new irregularity because it is outside the box. Please, regularize your account.',
			'IRREGULARITY_NOTICE_MESSAGE_TICKET_INCOMPATIVEL' => 'Your vehicle of plate {{plate}} received a new irregularity because ticket is incompatible. Please, regularize your account.',
			'IRREGULARITY_NOTICE_MESSAGE_SEM_TICKET' => 'Your vehicle of plate {{plate}} received a new irregularity because it has no ticket. Please, regularize your account.',
			'IRREGULARITY_NOTICE_MESSAGE_PERMANENCIA_EXCEDIDA' => 'Your vehicle of plate {{plate}} received a new irregularity because it stayed too long. Please, regularize your account.',
			'IRREGULARITY_NOTICE_MESSAGE_SEM_CREDENCIAL' => 'Your vehicle of plate {{plate}} received a new irregularity is has no credential. Please, regularize your account.',
			'IRREGULARITY_NOTICE_MESSAGE_TICKET_EXTERNO_SEM_CARTAO' => 'Your vehicle of plate {{plate}} received a new irregularity because it has no ticket. Please, regularize your account.',
			'IRREGULARITY_NOTICE_MESSAGE_TICKET_EXTERNO_CARTAO_IRREGULAR' => 'Your vehicle of plate {{plate}} received a new irregularity because it has a irregular ticket. Please, regularize your account.',
			'IRREGULARITY_NOTICE_MESSAGE_TICKET_EXTERNO_CARTAO_NAO_LEGIVEL' => 'Your vehicle of plate {{plate}} received a new irregularity because it has a unreadable ticket. Please, regularize your account.',
			'IRREGULARITY_NOTICE_MESSAGE_TICKET_EXTERNO_CARTAO_HORARIO_VENCIDO' => 'Your vehicle of plate {{plate}} received a new irregularity because it has a sold ticket. Please, regularize your account.',

			'RECHARGE_SUCCESS_TITLE' => 'Your recharge has been approved',
			'RECHARGE_SUCCESS_MESSAGE' => 'Your recharge has been approved. A credit of R${{value}} will be made to your account.',

			'TICKET_ACTIVATED_TITLE' => 'Ticket activated',
			'TICKET_ACTIVATED_MESSAGE' => 'A ticket of R${{value}}, valid until {{endTime}}, was activated for license plate {{plate}}.',

			'TICKET_DEACTIVATED_TITLE' => 'Ticket deactivated',
			'TICKET_DEACTIVATED_MESSAGE' => 'A ticket of R${{value}} was deactivated for license plate {{plate}}.',

			'TICKET_CANCELED_TITLE' => 'Ticket canceled',
			'TICKET_CANCELED_MESSAGE' => 'A ticket of R${{value}} was canceled for license plate {{plate}}.',

			'TICKET_ENDING_TITLE' => 'Ticket ending reminder',
			'TICKET_ENDING_MESSAGE' => 'The ticket for the license plate {{plate}} will expire at {{endTime}}.',

			'PLATE_EXCEEDING_TITLE' => 'Exceeding plate reminder',
			'PLATE_EXCEEDING_MESSAGE' => "The vehicle of plate {{plate}} will exceed the maximum time on a spot at {{exceedTime}}. Please, move it another spot and avoid irregularities.",
		],
	];

	public function __construct($language) {

		if (!array_key_exists($language, $this->strings))
			$language = 'pt-BR';

		$this->language = $language;

	}

	public function t($key, $params = []) {

		if (!array_key_exists($key, $this->strings[$this->language]))
			return $key;

		$str = $this->strings[$this->language][$key];
		foreach ($params as $key => $value)
			$str = str_replace('{{' . $key . '}}', $value, $str);

		return $str;

	}

}

?>
