<?php

namespace ParkingNotifications\Notification;

use Google\Auth\ApplicationDefaultCredentials;
use GuzzleHttp\Client;
use Google_Client;
use ParkingNotifications\Notification\Translator;

class Requester {

	const FIREBASE_MESSAGING_SCOPE = 'https://www.googleapis.com/auth/firebase.messaging';

	private $projectKey;

	public $client;

	public $origin;

	public $connection;

	public function guzzleClient($params) {

		return new Client($params);

	}

	public function googleClient() {

		return new Google_Client();

	}

	public function translator($language) {

		return new Translator($language);

	}

	public function __construct($connection, $origin) {

		$this->origin = $origin;

		$this->connection = $connection;

		$this->client = $this->guzzleClient([
			'base_uri' => NOTIFICATION_URI
		]);

	}

	private function setUpGoogleAuth() {

		putenv('GOOGLE_APPLICATION_CREDENTIALS=' . GOOGLE_SERVICES_FILE_DIR . 'google-services.json');

		$client = $this->googleClient();
		$client->useApplicationDefaultCredentials();
		$client->setScopes([self::FIREBASE_MESSAGING_SCOPE]);

		$client->refreshTokenWithAssertion();

		$token = $client->getAccessToken();

		$this->projectKey = $token['access_token'];

	}

	private function getDevicesByPlateAndVehicleType($plate, $vehicleType) {

		$query = "SELECT entidade_id as entity_id
                FROM park_placa
                WHERE placa = ?
                	AND inativo = 0
                	AND tipo = ?";

        $stmt = $this->connection->prepare($query);
        $stmt->bindValue(1, $plate);
        $stmt->bindValue(2, $vehicleType);
        $stmt->execute();

        $entities = $stmt->fetchAll();

        $devices = [];

        foreach ($entities as $entity)
        	$devices = array_merge($devices, $this->getDevicesByEntityId($entity['entity_id']));

		return $devices;

	}

	private function getDevicesByEntityId($entityId) {

		$query = "SELECT token, idioma as language
                FROM entidade_token_notificacoes
                WHERE entidade_id = ?";

        $stmt = $this->connection->prepare($query);
        $stmt->bindValue(1, $entityId);
        $stmt->execute();

        return $stmt->fetchAll();

	}

	private function deleteToken($token) {

		$query = "DELETE FROM entidade_token_notificacoes
                WHERE token = ?";

        $stmt = $this->connection->prepare($query);
        $stmt->bindValue(1, $token);
        $stmt->execute();

	}

	public function sendByPlateAndVehicleType($plate, $vehicleType, $title, $message, $tag, $translatorParams = []) {

		$devices = $this->getDevicesByPlateAndVehicleType($plate, $vehicleType);

		if(sizeof($devices) == 0)
			return;

		$this->executeFirebaseRequest($devices, $title, $message, $tag, $translatorParams);

	}

	public function sendByEntityId($entityId, $title, $message, $tag, $translatorParams = []) {

		$devices = $this->getDevicesByEntityId($entityId);

		if(sizeof($devices) == 0)
			return;

		$this->executeFirebaseRequest($devices, $title, $message, $tag, $translatorParams);

	}

	private function executeFirebaseRequest($devices, $title, $message, $tag, $translatorParams) {

		try {
			$this->setUpGoogleAuth();
		} catch (\Exception $e) {
			return;
		}

		$params = [
			'headers' => [
				'Authorization' => 'Bearer ' . $this->projectKey
			],
			'json' => [
				'message' => [
					'token' => null,
					'notification' => [
						'title' => null,
						'body' => null
					],
					'data' => [
						'origin' => $this->origin,
						'tag' => $tag,
					]
				]
			]
		];

		foreach ($devices as $device) {

			$params['json']['message']['token'] = $device['token'];

			$t = $this->translator($device['language']);
			$params['json']['message']['notification']['title'] = $t->t($title, $translatorParams);
			$params['json']['message']['notification']['body'] = $t->t($message, $translatorParams);

			try {
				$this->client->request('POST', '', $params);
			} catch (\Exception $e) {
				// Valida se a exception ocorrida foi por algum erro de client no request
    			if ($e instanceof \GuzzleHttp\Exception\ClientException) {
	    			$response = json_decode($e->getResponse()->getBody()->getContents(), true);
	    			if ($response['error']['code'] == 404) {
	    				foreach ($response['error']['details'] as $error) {
	    					// Valida se o erro foi porque o token enviado não está mais em funcionamento
	    					if ($error['errorCode'] == 'UNREGISTERED') {
	    						$this->deleteToken($device['token']);
	    						break;
	    					}
	    				}
	    			}
    			}
			}

		}

	}

}

?>
