<?php

namespace ParkingNotifications;

require_once ('Constants.php');

use ParkingNotifications\Notification\Creator;
use ParkingNotifications\Parking\Checker;

class Application {

	public $notificationCreator;

	public $parkingChecker;

	public function creator($connection, $origin) {

		return new Creator($connection, $origin);

	}

	public function checker($connection, $notificationCreator) {

		return new Checker($connection, $notificationCreator);

	}

	/**
	 * @param PDO  $connection PDO Object to connect in parking database
	 */
	public function __construct($connection, $origin) {

		$this->notificationCreator = $this->creator($connection, $origin);

		$this->parkingChecker = $this->checker($connection, $this->notificationCreator);

	}

}

?>
