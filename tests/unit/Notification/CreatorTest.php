<?php

use ParkingNotificationsTests\BaseTest;
use ParkingNotifications\Notification\Creator;
use ParkingNotifications\Notification\Requester;

class CreatorTest extends BaseTest {

	public function testConstruct() {

		$this->mock('ParkingNotifications\Notification\Creator', ['requester']);

		$firebaseRequester = $this->getClassMock('ParkingNotifications\Notification\Requester');

		$this->instance->expects($this->once())
			->method('requester')
			->with('CONNECTION', 'ORIGIN')
			->will($this->returnValue($firebaseRequester));

		$this->instance->__construct('CONNECTION', 'ORIGIN');

		$this->assertEquals($firebaseRequester, $this->instance->firebaseRequester);

	}

	public function testRemainingBalance() {

		$this->mock('ParkingNotifications\Notification\Creator');

		$firebaseRequester = $this->getClassMock('ParkingNotifications\Notification\Requester', ['sendByEntityId']);
		$firebaseRequester->expects($this->once())
			->method('sendByEntityId')
			->with('ENTITY_ID', 'REMAINING_BALANCE_TITLE', 'REMAINING_BALANCE_MESSAGE', 'remaining_balance');

		$this->instance->firebaseRequester = $firebaseRequester;

		$this->instance->remainingBalance('ENTITY_ID');

	}

	public function testIrregularityNotice() {

		$this->mock('ParkingNotifications\Notification\Creator');

		$firebaseRequester = $this->getClassMock('ParkingNotifications\Notification\Requester', ['sendByPlateAndVehicleType']);
		$firebaseRequester->expects($this->once())
			->method('sendByPlateAndVehicleType')
			->with('AAA-1111', 'CARRO', 'IRREGULARITY_NOTICE_TITLE', 'IRREGULARITY_NOTICE_MESSAGE_FORA_DA_VAGA', 'irregularity_notice', ['plate' => 'AAA-1111']);

		$this->instance->firebaseRequester = $firebaseRequester;

		$this->instance->irregularityNotice('AAA-1111', 'CARRO', 'FORA_DA_VAGA');

	}

	public function testRechargeSuccess() {

		$this->mock('ParkingNotifications\Notification\Creator');

		$firebaseRequester = $this->getClassMock('ParkingNotifications\Notification\Requester', ['sendByEntityId']);
		$firebaseRequester->expects($this->once())
			->method('sendByEntityId')
			->with('ENTITY_ID', 'RECHARGE_SUCCESS_TITLE', 'RECHARGE_SUCCESS_MESSAGE', 'recharge_success', ['value' => '12,34']);

		$this->instance->firebaseRequester = $firebaseRequester;

		$this->instance->rechargeSuccess('ENTITY_ID', '12,34');

	}

	public function testTicketActivated() {

		$this->mock('ParkingNotifications\Notification\Creator');

		$firebaseRequester = $this->getClassMock('ParkingNotifications\Notification\Requester', ['sendByPlateAndVehicleType']);
		$firebaseRequester->expects($this->once())
			->method('sendByPlateAndVehicleType')
			->with('AAA-1111', 'CARRO', 'TICKET_ACTIVATED_TITLE', 'TICKET_ACTIVATED_MESSAGE', 'ticket_activated', [
				'value' => '12,34',
				'plate' => 'AAA-1111',
				'endTime' => '12:34'
			]);

		$this->instance->firebaseRequester = $firebaseRequester;

		$this->instance->ticketActivated('AAA-1111', 'CARRO', '12:34', '12,34');

	}

	public function testTicketDeactivated() {

		$this->mock('ParkingNotifications\Notification\Creator');

		$firebaseRequester = $this->getClassMock('ParkingNotifications\Notification\Requester', ['sendByPlateAndVehicleType']);
		$firebaseRequester->expects($this->once())
			->method('sendByPlateAndVehicleType')
			->with('AAA-1111', 'CARRO', 'TICKET_DEACTIVATED_TITLE', 'TICKET_DEACTIVATED_MESSAGE', 'ticket_deactivated', [
				'value' => '12,34',
				'plate' => 'AAA-1111'
			]);

		$this->instance->firebaseRequester = $firebaseRequester;

		$this->instance->ticketDeactivated('AAA-1111', 'CARRO', '12,34');

	}

	public function testTicketCanceled() {

		$this->mock('ParkingNotifications\Notification\Creator');

		$firebaseRequester = $this->getClassMock('ParkingNotifications\Notification\Requester', ['sendByPlateAndVehicleType']);
		$firebaseRequester->expects($this->once())
			->method('sendByPlateAndVehicleType')
			->with('AAA-1111', 'CARRO', 'TICKET_CANCELED_TITLE', 'TICKET_CANCELED_MESSAGE', 'ticket_canceled', [
				'value' => '12,34',
				'plate' => 'AAA-1111'
			]);

		$this->instance->firebaseRequester = $firebaseRequester;

		$this->instance->ticketCanceled('AAA-1111', 'CARRO', '12,34');

	}

	public function testTicketEnding() {

		$this->mock('ParkingNotifications\Notification\Creator');

		$firebaseRequester = $this->getClassMock('ParkingNotifications\Notification\Requester', ['sendByPlateAndVehicleType']);
		$firebaseRequester->expects($this->once())
			->method('sendByPlateAndVehicleType')
			->with('AAA-1111', 'CARRO', 'TICKET_ENDING_TITLE', 'TICKET_ENDING_MESSAGE', 'ticket_ending', [
				'plate' => 'AAA-1111',
				'endTime' => 'END_TIME'
			]);

		$this->instance->firebaseRequester = $firebaseRequester;

		$this->instance->ticketEnding('AAA-1111', 'CARRO', 'END_TIME');

	}

	public function testPlateExceeding() {

		$this->mock('ParkingNotifications\Notification\Creator');

		$firebaseRequester = $this->getClassMock('ParkingNotifications\Notification\Requester', ['sendByPlateAndVehicleType']);
		$firebaseRequester->expects($this->once())
			->method('sendByPlateAndVehicleType')
			->with('AAA-1111', 'CARRO', 'PLATE_EXCEEDING_TITLE', 'PLATE_EXCEEDING_MESSAGE', 'plate_exceeding', [
				'plate' => 'AAA-1111',
				'exceedTime' => 'END_TIME'
			]);

		$this->instance->firebaseRequester = $firebaseRequester;

		$this->instance->plateExceeding('AAA-1111', 'CARRO', 'END_TIME');

	}

}

?>
