<?php

use ParkingNotificationsTests\BaseTest;
use ParkingNotifications\Notification\Translator;

class TranslatorTest extends BaseTest {

	public function testConstructUnknownLanguage() {

		$this->mock('ParkingNotifications\Notification\Translator');

		$this->instance->__construct('unknown');

		$this->assertEquals('pt-BR', $this->instance->language);

	}

	public function testConstruct() {

		$this->mock('ParkingNotifications\Notification\Translator');

		$this->instance->__construct('en-US');

		$this->assertEquals('en-US', $this->instance->language);

	}

	public function testTKeyNotExists() {

		$this->mock('ParkingNotifications\Notification\Translator');

		$this->instance->language = 'pt-BR';

		$result = $this->instance->t('KEY_NOT_EXISTS');

		$this->assertEquals('KEY_NOT_EXISTS', $result);

	}

	public function testTKeyExists() {

		$this->mock('ParkingNotifications\Notification\Translator');

		$this->instance->language = 'pt-BR';

		$result = $this->instance->t('REMAINING_BALANCE_TITLE');

		$this->assertEquals('Seu saldo está terminando', $result);

	}

	public function testTKeyExistsWithParams() {

		$this->mock('ParkingNotifications\Notification\Translator');

		$this->instance->language = 'pt-BR';

		$result = $this->instance->t('RECHARGE_SUCCESS_MESSAGE', ['value' => 'VALUE']);

		$this->assertEquals('Uma recarga de R$VALUE foi aprovada na sua conta.', $result);

	}

}

?>