<?php

use ParkingNotificationsTests\BaseTest;
use ParkingNotifications\Notification\Requester;
use GuzzleHttp\Client;
use Google\Google_Client;
use ParkingNotifications\Notification\Translator;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Stream;

class RequesterTest extends BaseTest {

	public static function setUpBeforeClass() {

		define('GOOGLE_SERVICES_FILE_DIR', 'GOOGLE_SERVICES_FILE_DIR');

	}

	public function testConstruct() {

		$this->mock('ParkingNotifications\Notification\Requester', ['guzzleClient']);

		$client = $this->getClassMock('GuzzleHttp\Client');
		$this->instance->expects($this->once())
			->method('guzzleClient')
			->with([
				'base_uri' => 'https://fcm.googleapis.com/v1/projects/parking-1f32f/messages:send'
			])
			->will($this->returnValue($client));

		$this->instance->__construct('CONNECTION', 'ORIGIN');

		$this->assertEquals('ORIGIN', $this->instance->origin);
		$this->assertEquals('CONNECTION', $this->instance->connection);
		$this->assertEquals($client, $this->instance->client);

	}

	public function testSendByEntityIdWithNoDevicesFound() {

		$this->mock('ParkingNotifications\Notification\Requester', ['googleClient']);

		$this->instance->connection = $this->getClassMock('PDO', ['prepare']);

		$stmt = $this->getClassMock('PDOStatement', ['bindValue', 'execute', 'fetchAll']);
		$stmt->expects($this->once())
			->method('bindValue')
			->with(1, 'ENTITY_ID');

		$stmt->expects($this->once())
			->method('execute');
		$stmt->expects($this->once())
			->method('fetchAll')
			->will($this->returnValue([]));

		$query = "SELECT token, idioma as language
                FROM entidade_token_notificacoes
                WHERE entidade_id = ?";
		$this->instance->connection->expects($this->once())
			->method('prepare')
			->with($query)
			->will($this->returnValue($stmt));

		$this->instance->sendByEntityId('ENTITY_ID', 'TITLE', 'MESSAGE', 'TAG');

	}

	public function testSendByEntityId() {

		$this->mock('ParkingNotifications\Notification\Requester', ['googleClient', 'translator']);

		$googleClient = $this->getClassMock('Google_Client', ['useApplicationDefaultCredentials', 'setScopes', 'refreshTokenWithAssertion', 'getAccessToken']);
		$googleClient->expects($this->once())
			->method('getAccessToken')
			->will($this->returnValue(['access_token' => 'AUTH_TOKEN']));
		$this->instance->expects($this->once())
			->method('googleClient')
			->will($this->returnValue($googleClient));

		$this->instance->connection = $this->getClassMock('PDO', ['prepare']);

		$stmt = $this->getClassMock('PDOStatement', ['bindValue', 'execute', 'fetchAll']);
		$stmt->expects($this->once())
			->method('fetchAll')
			->will($this->returnValue([
				[
					'token' => 'TOKEN',
					'language' => 'pt-BR'
				]
			]));

		$this->instance->connection->expects($this->once())
			->method('prepare')
			->will($this->returnValue($stmt));

		$translator = $this->getClassMock('ParkingNotifications\Notification\Translator', ['t']);
		$translator->expects($this->at(0))
			->method('t')
			->with('TITLE')
			->will($this->returnValue('TRANSLATED_TITLE'));
		$translator->expects($this->at(1))
			->method('t')
			->with('MESSAGE')
			->will($this->returnValue('TRANSLATED_MESSAGE'));

		$this->instance->expects($this->once())
			->method('translator')
			->with('pt-BR')
			->will($this->returnValue($translator));

		$this->instance->origin = 'ORIGIN';

		$this->instance->client = $this->getClassMock('GuzzleHttp\Client', ['request']);
		$this->instance->client->expects($this->once())
			->method('request')
			->with('POST', '', [
				'headers' => [
					'Authorization' => 'Bearer AUTH_TOKEN'
				],
				'json' => [
					'message' => [
						'token' => 'TOKEN',
						'notification' => [
							'title' => 'TRANSLATED_TITLE',
							'body' => 'TRANSLATED_MESSAGE'
						],
						'data' => [
							'origin' => 'ORIGIN',
							'tag' => 'TAG'
						]
					]
				]
			]);

		$this->instance->sendByEntityId('ENTITY_ID', 'TITLE', 'MESSAGE', 'TAG');

	}

	public function testSendByEntityIdWithTransalatorParams() {

		$this->mock('ParkingNotifications\Notification\Requester', ['googleClient', 'translator']);

		$googleClient = $this->getClassMock('Google_Client', ['useApplicationDefaultCredentials', 'setScopes', 'refreshTokenWithAssertion', 'getAccessToken']);
		$googleClient->expects($this->once())
			->method('getAccessToken')
			->will($this->returnValue(['access_token' => 'AUTH_TOKEN']));
		$this->instance->expects($this->once())
			->method('googleClient')
			->will($this->returnValue($googleClient));

		$this->instance->connection = $this->getClassMock('PDO', ['prepare']);

		$stmt = $this->getClassMock('PDOStatement', ['bindValue', 'execute', 'fetchAll']);
		$stmt->expects($this->once())
			->method('fetchAll')
			->will($this->returnValue([
				[
					'token' => 'TOKEN',
					'language' => 'pt-BR'
				]
			]));

		$this->instance->connection->expects($this->once())
			->method('prepare')
			->will($this->returnValue($stmt));

		$translator = $this->getClassMock('ParkingNotifications\Notification\Translator', ['t']);
		$translator->expects($this->at(0))
			->method('t')
			->with('TITLE', ['TRANSLATOR_PARAM'])
			->will($this->returnValue('TRANSLATED_TITLE'));
		$translator->expects($this->at(1))
			->method('t')
			->with('MESSAGE', ['TRANSLATOR_PARAM'])
			->will($this->returnValue('TRANSLATED_MESSAGE'));

		$this->instance->expects($this->once())
			->method('translator')
			->with('pt-BR')
			->will($this->returnValue($translator));

		$this->instance->origin = 'ORIGIN';

		$this->instance->client = $this->getClassMock('GuzzleHttp\Client', ['request']);
		$this->instance->client->expects($this->once())
			->method('request')
			->with('POST', '', [
				'headers' => [
					'Authorization' => 'Bearer AUTH_TOKEN'
				],
				'json' => [
					'message' => [
						'token' => 'TOKEN',
						'notification' => [
							'title' => 'TRANSLATED_TITLE',
							'body' => 'TRANSLATED_MESSAGE'
						],
						'data' => [
							'origin' => 'ORIGIN',
							'tag' => 'TAG'
						]
					]
				]
			]);

		$this->instance->sendByEntityId('ENTITY_ID', 'TITLE', 'MESSAGE', 'TAG', ['TRANSLATOR_PARAM']);

	}

	public function testSendByEntityIdMultipleDevices() {

		$this->mock('ParkingNotifications\Notification\Requester', ['googleClient', 'translator']);

		$googleClient = $this->getClassMock('Google_Client', ['useApplicationDefaultCredentials', 'setScopes', 'refreshTokenWithAssertion', 'getAccessToken']);
		$googleClient->expects($this->once())
			->method('getAccessToken')
			->will($this->returnValue(['access_token' => 'AUTH_TOKEN']));
		$this->instance->expects($this->once())
			->method('googleClient')
			->will($this->returnValue($googleClient));

		$this->instance->connection = $this->getClassMock('PDO', ['prepare']);

		$stmt = $this->getClassMock('PDOStatement', ['bindValue', 'execute', 'fetchAll']);
		$stmt->expects($this->once())
			->method('fetchAll')
			->will($this->returnValue([
				[
					'token' => 'TOKEN1',
					'language' => 'LANGUAGE'
				],
				[
					'token' => 'TOKEN2',
					'language' => 'LANGUAGE'
				]
			]));

		$this->instance->connection->expects($this->once())
			->method('prepare')
			->will($this->returnValue($stmt));

		$translator = $this->getClassMock('ParkingNotifications\Notification\Translator', ['t']);
		$translator->expects($this->at(0))
			->method('t')
			->with('TITLE')
			->will($this->returnValue('TRANSLATED_TITLE'));
		$translator->expects($this->at(1))
			->method('t')
			->with('MESSAGE')
			->will($this->returnValue('TRANSLATED_MESSAGE'));
		$translator->expects($this->at(2))
			->method('t')
			->with('TITLE')
			->will($this->returnValue('TRANSLATED_TITLE'));
		$translator->expects($this->at(3))
			->method('t')
			->with('MESSAGE')
			->will($this->returnValue('TRANSLATED_MESSAGE'));

		$this->instance->expects($this->exactly(2))
			->method('translator')
			->with('LANGUAGE')
			->will($this->returnValue($translator));

		$this->instance->origin = 'ORIGIN';

		$this->instance->client = $this->getClassMock('GuzzleHttp\Client', ['request']);
		$this->instance->client->expects($this->at(0))
			->method('request')
			->with('POST', '', [
				'headers' => [
					'Authorization' => 'Bearer AUTH_TOKEN'
				],
				'json' => [
					'message' => [
						'token' => 'TOKEN1',
						'notification' => [
							'title' => 'TRANSLATED_TITLE',
							'body' => 'TRANSLATED_MESSAGE'
						],
						'data' => [
							'origin' => 'ORIGIN',
							'tag' => 'TAG'
						]
					]
				]
			]);
		$this->instance->client->expects($this->at(1))
			->method('request')
			->with('POST', '', [
				'headers' => [
					'Authorization' => 'Bearer AUTH_TOKEN'
				],
				'json' => [
					'message' => [
						'token' => 'TOKEN2',
						'notification' => [
							'title' => 'TRANSLATED_TITLE',
							'body' => 'TRANSLATED_MESSAGE'
						],
						'data' => [
							'origin' => 'ORIGIN',
							'tag' => 'TAG'
						]
					]
				]
			]);

		$this->instance->sendByEntityId('ENTITY_ID', 'TITLE', 'MESSAGE', 'TAG');

	}

	public function testSendByEntityIdUnknownException() {

		$this->mock('ParkingNotifications\Notification\Requester', ['googleClient', 'translator']);

		$googleClient = $this->getClassMock('Google_Client', ['useApplicationDefaultCredentials', 'setScopes', 'refreshTokenWithAssertion', 'getAccessToken']);
		$googleClient->expects($this->once())
			->method('getAccessToken')
			->will($this->returnValue(['access_token' => 'AUTH_TOKEN']));
		$this->instance->expects($this->once())
			->method('googleClient')
			->will($this->returnValue($googleClient));

		$this->instance->connection = $this->getClassMock('PDO', ['prepare']);

		$stmt = $this->getClassMock('PDOStatement', ['bindValue', 'execute', 'fetchAll']);
		$stmt->expects($this->once())
			->method('fetchAll')
			->will($this->returnValue([
				[
					'token' => 'TOKEN',
					'language' => 'pt-BR'
				]
			]));

		$this->instance->connection->expects($this->once())
			->method('prepare')
			->will($this->returnValue($stmt));

		$translator = $this->getClassMock('ParkingNotifications\Notification\Translator', ['t']);
		$translator->expects($this->at(0))
			->method('t')
			->with('TITLE')
			->will($this->returnValue('TRANSLATED_TITLE'));
		$translator->expects($this->at(1))
			->method('t')
			->with('MESSAGE')
			->will($this->returnValue('TRANSLATED_MESSAGE'));

		$this->instance->expects($this->once())
			->method('translator')
			->with('pt-BR')
			->will($this->returnValue($translator));

		$this->instance->origin = 'ORIGIN';

		$this->instance->client = $this->getClassMock('GuzzleHttp\Client', ['request']);
		$this->instance->client->expects($this->once())
			->method('request')
			->will($this->throwException(new Exception('Unknown exception')));

		$this->instance->sendByEntityId('ENTITY_ID', 'TITLE', 'MESSAGE', 'TAG');

	}

	public function testSendByEntityIdClientExceptionUnregisteredToken() {

		$this->mock('ParkingNotifications\Notification\Requester', ['googleClient', 'translator']);

		$googleClient = $this->getClassMock('Google_Client', ['useApplicationDefaultCredentials', 'setScopes', 'refreshTokenWithAssertion', 'getAccessToken']);
		$googleClient->expects($this->once())
			->method('getAccessToken')
			->will($this->returnValue(['access_token' => 'AUTH_TOKEN']));
		$this->instance->expects($this->once())
			->method('googleClient')
			->will($this->returnValue($googleClient));

		$this->instance->connection = $this->getClassMock('PDO', ['prepare']);

		$stmt1 = $this->getClassMock('PDOStatement', ['bindValue', 'execute', 'fetchAll']);
		$stmt1->expects($this->once())
			->method('fetchAll')
			->will($this->returnValue([
				[
					'token' => 'TOKEN',
					'language' => 'pt-BR'
				]
			]));

		$this->instance->connection->expects($this->at(0))
			->method('prepare')
			->will($this->returnValue($stmt1));


		$stmt2 = $this->getClassMock('PDOStatement', ['bindValue', 'execute']);
		$stmt2->expects($this->once())
			->method('bindValue');
		$stmt2->expects($this->once())
			->method('execute');

		$query = "DELETE FROM entidade_token_notificacoes
                WHERE token = ?";

		$this->instance->connection->expects($this->at(1))
			->method('prepare')
			->with($query)
			->will($this->returnValue($stmt2));

		$translator = $this->getClassMock('ParkingNotifications\Notification\Translator', ['t']);
		$translator->expects($this->at(0))
			->method('t')
			->with('TITLE')
			->will($this->returnValue('TRANSLATED_TITLE'));
		$translator->expects($this->at(1))
			->method('t')
			->with('MESSAGE')
			->will($this->returnValue('TRANSLATED_MESSAGE'));

		$this->instance->expects($this->once())
			->method('translator')
			->with('pt-BR')
			->will($this->returnValue($translator));

		$this->instance->origin = 'ORIGIN';

		$clientException = $this->getClassMock('GuzzleHttp\Exception\ClientException', ['getResponse']);
		$responseInterface = $this->getClassMock('GuzzleHttp\Psr7\Response', ['getBody']);
		$streamInterface = $this->getClassMock('GuzzleHttp\Psr7\Stream', ['getContents']);

		$streamInterface->expects($this->once())
			->method('getContents')
			->will($this->returnValue('{"error":{"code": 404,"details":[{"errorCode": "UNREGISTERED"}]}}'));

		$responseInterface->expects($this->once())
			->method('getBody')
			->will($this->returnValue($streamInterface));

		$clientException->expects($this->once())
			->method('getResponse')
			->will($this->returnValue($responseInterface));

		$this->instance->client = $this->getClassMock('GuzzleHttp\Client', ['request']);
		$this->instance->client->expects($this->once())
			->method('request')
			->will($this->throwException($clientException));

		$this->instance->sendByEntityId('ENTITY_ID', 'TITLE', 'MESSAGE', 'TAG');

	}

	public function testSendByEntityIdUnexpectedClientException() {

		$this->mock('ParkingNotifications\Notification\Requester', ['googleClient', 'translator']);

		$googleClient = $this->getClassMock('Google_Client', ['useApplicationDefaultCredentials', 'setScopes', 'refreshTokenWithAssertion', 'getAccessToken']);
		$googleClient->expects($this->once())
			->method('getAccessToken')
			->will($this->returnValue(['access_token' => 'AUTH_TOKEN']));
		$this->instance->expects($this->once())
			->method('googleClient')
			->will($this->returnValue($googleClient));

		$this->instance->connection = $this->getClassMock('PDO', ['prepare']);

		$stmt1 = $this->getClassMock('PDOStatement', ['bindValue', 'execute', 'fetchAll']);
		$stmt1->expects($this->once())
			->method('fetchAll')
			->will($this->returnValue([
				[
					'token' => 'TOKEN',
					'language' => 'pt-BR'
				]
			]));

		$this->instance->connection->expects($this->once())
			->method('prepare')
			->will($this->returnValue($stmt1));

		$translator = $this->getClassMock('ParkingNotifications\Notification\Translator', ['t']);
		$translator->expects($this->at(0))
			->method('t')
			->with('TITLE')
			->will($this->returnValue('TRANSLATED_TITLE'));
		$translator->expects($this->at(1))
			->method('t')
			->with('MESSAGE')
			->will($this->returnValue('TRANSLATED_MESSAGE'));

		$this->instance->expects($this->once())
			->method('translator')
			->with('pt-BR')
			->will($this->returnValue($translator));

		$this->instance->origin = 'ORIGIN';

		$clientException = $this->getClassMock('GuzzleHttp\Exception\ClientException', ['getResponse']);
		$responseInterface = $this->getClassMock('GuzzleHttp\Psr7\Response', ['getBody']);
		$streamInterface = $this->getClassMock('GuzzleHttp\Psr7\Stream', ['getContents']);

		$streamInterface->expects($this->once())
			->method('getContents')
			->will($this->returnValue('{"error":{"code": 400,"details":[{"errorCode": "UNREGISTERED"}]}}'));

		$responseInterface->expects($this->once())
			->method('getBody')
			->will($this->returnValue($streamInterface));

		$clientException->expects($this->once())
			->method('getResponse')
			->will($this->returnValue($responseInterface));

		$this->instance->client = $this->getClassMock('GuzzleHttp\Client', ['request']);
		$this->instance->client->expects($this->once())
			->method('request')
			->will($this->throwException($clientException));

		$this->instance->sendByEntityId('ENTITY_ID', 'TITLE', 'MESSAGE', 'TAG');

	}

	public function testSendByEntityIdUnexpectedClientExceptionError() {

		$this->mock('ParkingNotifications\Notification\Requester', ['googleClient', 'translator']);

		$googleClient = $this->getClassMock('Google_Client', ['useApplicationDefaultCredentials', 'setScopes', 'refreshTokenWithAssertion', 'getAccessToken']);
		$googleClient->expects($this->once())
			->method('getAccessToken')
			->will($this->returnValue(['access_token' => 'AUTH_TOKEN']));
		$this->instance->expects($this->once())
			->method('googleClient')
			->will($this->returnValue($googleClient));

		$this->instance->connection = $this->getClassMock('PDO', ['prepare']);

		$stmt1 = $this->getClassMock('PDOStatement', ['bindValue', 'execute', 'fetchAll']);
		$stmt1->expects($this->once())
			->method('fetchAll')
			->will($this->returnValue([
				[
					'token' => 'TOKEN',
					'language' => 'pt-BR'
				]
			]));

		$this->instance->connection->expects($this->at(0))
			->method('prepare')
			->will($this->returnValue($stmt1));

		$translator = $this->getClassMock('ParkingNotifications\Notification\Translator', ['t']);
		$translator->expects($this->at(0))
			->method('t')
			->with('TITLE')
			->will($this->returnValue('TRANSLATED_TITLE'));
		$translator->expects($this->at(1))
			->method('t')
			->with('MESSAGE')
			->will($this->returnValue('TRANSLATED_MESSAGE'));

		$this->instance->expects($this->once())
			->method('translator')
			->with('pt-BR')
			->will($this->returnValue($translator));

		$this->instance->origin = 'ORIGIN';

		$clientException = $this->getClassMock('GuzzleHttp\Exception\ClientException', ['getResponse']);
		$responseInterface = $this->getClassMock('GuzzleHttp\Psr7\Response', ['getBody']);
		$streamInterface = $this->getClassMock('GuzzleHttp\Psr7\Stream', ['getContents']);

		$streamInterface->expects($this->once())
			->method('getContents')
			->will($this->returnValue('{"error":{"code": 404,"details":[{"errorCode": "UNEXPECTED"}]}}'));

		$responseInterface->expects($this->once())
			->method('getBody')
			->will($this->returnValue($streamInterface));

		$clientException->expects($this->once())
			->method('getResponse')
			->will($this->returnValue($responseInterface));

		$this->instance->client = $this->getClassMock('GuzzleHttp\Client', ['request']);
		$this->instance->client->expects($this->once())
			->method('request')
			->will($this->throwException($clientException));

		$this->instance->sendByEntityId('ENTITY_ID', 'TITLE', 'MESSAGE', 'TAG');

	}

	public function testSendByPlateAndVehicleType() {

		$this->mock('ParkingNotifications\Notification\Requester', ['googleClient', 'translator']);

		$googleClient = $this->getClassMock('Google_Client', ['useApplicationDefaultCredentials', 'setScopes', 'refreshTokenWithAssertion', 'getAccessToken']);
		$googleClient->expects($this->once())
			->method('getAccessToken')
			->will($this->returnValue(['access_token' => 'AUTH_TOKEN']));
		$this->instance->expects($this->once())
			->method('googleClient')
			->will($this->returnValue($googleClient));

		$this->instance->connection = $this->getClassMock('PDO', ['prepare']);

		$stmt = $this->getClassMock('PDOStatement', ['bindValue', 'execute', 'fetchAll']);
		$stmt->expects($this->at(3))
			->method('fetchAll')
			->will($this->returnValue([
				[
					'entity_id' => 'ENTITY1_ID',
				]
			]));

		$stmt->expects($this->at(6))
			->method('fetchAll')
			->will($this->returnValue([
				[
					'token' => 'TOKEN1',
					'language' => 'LANGUAGE'
				]
			]));

		$this->instance->connection->expects($this->exactly(2))
			->method('prepare')
			->will($this->returnValue($stmt));

		$translator = $this->getClassMock('ParkingNotifications\Notification\Translator', ['t']);
		$translator->expects($this->at(0))
			->method('t')
			->with('TITLE')
			->will($this->returnValue('TRANSLATED_TITLE'));
		$translator->expects($this->at(1))
			->method('t')
			->with('MESSAGE')
			->will($this->returnValue('TRANSLATED_MESSAGE'));

		$this->instance->expects($this->once())
			->method('translator')
			->with('LANGUAGE')
			->will($this->returnValue($translator));

		$this->instance->origin = 'ORIGIN';

		$this->instance->client = $this->getClassMock('GuzzleHttp\Client', ['request']);
		$this->instance->client->expects($this->at(0))
			->method('request')
			->with('POST', '', [
				'headers' => [
					'Authorization' => 'Bearer AUTH_TOKEN'
				],
				'json' => [
					'message' => [
						'token' => 'TOKEN1',
						'notification' => [
							'title' => 'TRANSLATED_TITLE',
							'body' => 'TRANSLATED_MESSAGE'
						],
						'data' => [
							'origin' => 'ORIGIN',
							'tag' => 'TAG'
						]
					]
				]
			]);

		$this->instance->sendByPlateAndVehicleType('AAA-1111', 'CARRO', 'TITLE', 'MESSAGE', 'TAG');

	}

	public function testSendByPlateAndVehicleTypeMultipleDevices() {

		$this->mock('ParkingNotifications\Notification\Requester', ['googleClient', 'translator']);

		$googleClient = $this->getClassMock('Google_Client', ['useApplicationDefaultCredentials', 'setScopes', 'refreshTokenWithAssertion', 'getAccessToken']);
		$googleClient->expects($this->once())
			->method('getAccessToken')
			->will($this->returnValue(['access_token' => 'AUTH_TOKEN']));
		$this->instance->expects($this->once())
			->method('googleClient')
			->will($this->returnValue($googleClient));

		$this->instance->connection = $this->getClassMock('PDO', ['prepare']);

		$stmt = $this->getClassMock('PDOStatement', ['bindValue', 'execute', 'fetchAll']);
		$stmt->expects($this->at(3))
			->method('fetchAll')
			->will($this->returnValue([
				[
					'entity_id' => 'ENTITY1_ID',
				], [
					'entity_id' => 'ENTITY2_ID',
				]
			]));

		$stmt->expects($this->at(6))
			->method('fetchAll')
			->will($this->returnValue([
				[
					'token' => 'TOKEN1',
					'language' => 'LANGUAGE'
				], [
					'token' => 'TOKEN2',
					'language' => 'LANGUAGE'
				]
			]));
		$stmt->expects($this->at(9))
			->method('fetchAll')
			->will($this->returnValue([
				[
					'token' => 'TOKEN3',
					'language' => 'LANGUAGE'
				], [
					'token' => 'TOKEN4',
					'language' => 'LANGUAGE'
				]
			]));

		$this->instance->connection->expects($this->exactly(3))
			->method('prepare')
			->will($this->returnValue($stmt));

		$translator = $this->getClassMock('ParkingNotifications\Notification\Translator', ['t']);
		$translator->expects($this->at(0))
			->method('t')
			->with('TITLE')
			->will($this->returnValue('TRANSLATED_TITLE'));
		$translator->expects($this->at(1))
			->method('t')
			->with('MESSAGE')
			->will($this->returnValue('TRANSLATED_MESSAGE'));
		$translator->expects($this->at(2))
			->method('t')
			->with('TITLE')
			->will($this->returnValue('TRANSLATED_TITLE'));
		$translator->expects($this->at(3))
			->method('t')
			->with('MESSAGE')
			->will($this->returnValue('TRANSLATED_MESSAGE'));
		$translator->expects($this->at(4))
			->method('t')
			->with('TITLE')
			->will($this->returnValue('TRANSLATED_TITLE'));
		$translator->expects($this->at(5))
			->method('t')
			->with('MESSAGE')
			->will($this->returnValue('TRANSLATED_MESSAGE'));
		$translator->expects($this->at(6))
			->method('t')
			->with('TITLE')
			->will($this->returnValue('TRANSLATED_TITLE'));
		$translator->expects($this->at(7))
			->method('t')
			->with('MESSAGE')
			->will($this->returnValue('TRANSLATED_MESSAGE'));

		$this->instance->expects($this->exactly(4))
			->method('translator')
			->with('LANGUAGE')
			->will($this->returnValue($translator));

		$this->instance->origin = 'ORIGIN';

		$this->instance->client = $this->getClassMock('GuzzleHttp\Client', ['request']);
		$this->instance->client->expects($this->at(0))
			->method('request')
			->with('POST', '', [
				'headers' => [
					'Authorization' => 'Bearer AUTH_TOKEN'
				],
				'json' => [
					'message' => [
						'token' => 'TOKEN1',
						'notification' => [
							'title' => 'TRANSLATED_TITLE',
							'body' => 'TRANSLATED_MESSAGE'
						],
						'data' => [
							'origin' => 'ORIGIN',
							'tag' => 'TAG'
						]
					]
				]
			]);
		$this->instance->client->expects($this->at(1))
			->method('request')
			->with('POST', '', [
				'headers' => [
					'Authorization' => 'Bearer AUTH_TOKEN'
				],
				'json' => [
					'message' => [
						'token' => 'TOKEN2',
						'notification' => [
							'title' => 'TRANSLATED_TITLE',
							'body' => 'TRANSLATED_MESSAGE'
						],
						'data' => [
							'origin' => 'ORIGIN',
							'tag' => 'TAG'
						]
					]
				]
			]);

		$this->instance->sendByPlateAndVehicleType('AAA-1111', 'CARRO', 'TITLE', 'MESSAGE', 'TAG');

	}

	public function testSendByPlateAndVehicleTypeWithPlateNotFound() {

		$this->mock('ParkingNotifications\Notification\Requester', ['translator']);

		$this->instance->connection = $this->getClassMock('PDO', ['prepare']);

		$stmt = $this->getClassMock('PDOStatement', ['bindValue', 'execute', 'fetchAll']);
		$stmt->expects($this->at(3))
			->method('fetchAll')
			->will($this->returnValue([]));

		$this->instance->connection->expects($this->exactly(1))
			->method('prepare')
			->will($this->returnValue($stmt));

		$translator = $this->getClassMock('ParkingNotifications\Notification\Translator', ['t']);
		$translator->expects($this->never())
			->method('t');
		$this->instance->expects($this->never())
			->method('translator');

		$this->instance->client = $this->getClassMock('GuzzleHttp\Client', ['request']);
		$this->instance->client->expects($this->never())
			->method('request');

		$this->instance->sendByPlateAndVehicleType('AAA-1111', 'CARRO', 'TITLE', 'MESSAGE', 'TAG');

	}

	public function testSendByPlateAndVehicleTypeWithNoDevicesFound() {

		$this->mock('ParkingNotifications\Notification\Requester', ['translator']);

		$this->instance->connection = $this->getClassMock('PDO', ['prepare']);

		$stmt = $this->getClassMock('PDOStatement', ['bindValue', 'execute', 'fetchAll']);
		$stmt->expects($this->at(3))
			->method('fetchAll')
			->will($this->returnValue([
				[
					'entity_id' => 'ENTITY1_ID',
				], [
					'entity_id' => 'ENTITY2_ID',
				]
			]));

		$stmt->expects($this->at(6))
			->method('fetchAll')
			->will($this->returnValue([]));
		$stmt->expects($this->at(9))
			->method('fetchAll')
			->will($this->returnValue([]));

		$this->instance->connection->expects($this->exactly(3))
			->method('prepare')
			->will($this->returnValue($stmt));

		$translator = $this->getClassMock('ParkingNotifications\Notification\Translator', ['t']);
		$translator->expects($this->never())
			->method('t');
		$this->instance->expects($this->never())
			->method('translator');

		$this->instance->client = $this->getClassMock('GuzzleHttp\Client', ['request']);
		$this->instance->client->expects($this->never())
			->method('request');

		$this->instance->sendByPlateAndVehicleType('AAA-1111', 'CARRO', 'TITLE', 'MESSAGE', 'TAG');

	}

	public function testSendByPlateAndVehicleTypeWithUnkownExpection() {

		$this->mock('ParkingNotifications\Notification\Requester', ['googleClient', 'translator']);

		$googleClient = $this->getClassMock('Google_Client', ['useApplicationDefaultCredentials', 'setScopes', 'refreshTokenWithAssertion', 'getAccessToken']);
		$googleClient->expects($this->once())
			->method('getAccessToken')
			->will($this->returnValue(['access_token' => 'AUTH_TOKEN']));
		$this->instance->expects($this->once())
			->method('googleClient')
			->will($this->returnValue($googleClient));

		$this->instance->connection = $this->getClassMock('PDO', ['prepare']);

		$stmt = $this->getClassMock('PDOStatement', ['bindValue', 'execute', 'fetchAll']);
		$stmt->expects($this->at(3))
			->method('fetchAll')
			->will($this->returnValue([
				[
					'entity_id' => 'ENTITY1_ID',
				]
			]));

		$stmt->expects($this->at(6))
			->method('fetchAll')
			->will($this->returnValue([
				[
					'token' => 'TOKEN1',
					'language' => 'LANGUAGE'
				]
			]));

		$this->instance->connection->expects($this->exactly(2))
			->method('prepare')
			->will($this->returnValue($stmt));

		$translator = $this->getClassMock('ParkingNotifications\Notification\Translator', ['t']);
		$translator->expects($this->at(0))
			->method('t')
			->with('TITLE')
			->will($this->returnValue('TRANSLATED_TITLE'));
		$translator->expects($this->at(1))
			->method('t')
			->with('MESSAGE')
			->will($this->returnValue('TRANSLATED_MESSAGE'));

		$this->instance->expects($this->once())
			->method('translator')
			->with('LANGUAGE')
			->will($this->returnValue($translator));

		$this->instance->origin = 'ORIGIN';

		$this->instance->client = $this->getClassMock('GuzzleHttp\Client', ['request']);
		$this->instance->client->expects($this->once())
			->method('request')
			->will($this->throwException(new Exception('Unknown exception')));

		$this->instance->sendByPlateAndVehicleType('AAA-1111', 'CARRO', 'TITLE', 'MESSAGE', 'TAG');

	}

	public function testSendByPlateAndVehicleTypeUnregisteredToken() {

		$this->mock('ParkingNotifications\Notification\Requester', ['googleClient', 'translator']);

		$googleClient = $this->getClassMock('Google_Client', ['useApplicationDefaultCredentials', 'setScopes', 'refreshTokenWithAssertion', 'getAccessToken']);
		$googleClient->expects($this->once())
			->method('getAccessToken')
			->will($this->returnValue(['access_token' => 'AUTH_TOKEN']));
		$this->instance->expects($this->once())
			->method('googleClient')
			->will($this->returnValue($googleClient));

		$this->instance->connection = $this->getClassMock('PDO', ['prepare']);

		$stmt = $this->getClassMock('PDOStatement', ['bindValue', 'execute', 'fetchAll']);
		$stmt->expects($this->at(3))
			->method('fetchAll')
			->will($this->returnValue([
				[
					'entity_id' => 'ENTITY1_ID',
				]
			]));

		$stmt->expects($this->at(6))
			->method('fetchAll')
			->will($this->returnValue([
				[
					'token' => 'TOKEN1',
					'language' => 'LANGUAGE'
				]
			]));

		$this->instance->connection->expects($this->at(0))
			->method('prepare')
			->will($this->returnValue($stmt));

		$this->instance->connection->expects($this->at(1))
			->method('prepare')
			->will($this->returnValue($stmt));

		$stmt2 = $this->getClassMock('PDOStatement', ['bindValue', 'execute']);
		$stmt2->expects($this->once())
			->method('bindValue');
		$stmt2->expects($this->once())
			->method('execute');

		$query = "DELETE FROM entidade_token_notificacoes
                WHERE token = ?";

		$this->instance->connection->expects($this->at(2))
			->method('prepare')
			->with($query)
			->will($this->returnValue($stmt2));

		$translator = $this->getClassMock('ParkingNotifications\Notification\Translator', ['t']);
		$translator->expects($this->at(0))
			->method('t')
			->with('TITLE')
			->will($this->returnValue('TRANSLATED_TITLE'));
		$translator->expects($this->at(1))
			->method('t')
			->with('MESSAGE')
			->will($this->returnValue('TRANSLATED_MESSAGE'));

		$this->instance->expects($this->once())
			->method('translator')
			->with('LANGUAGE')
			->will($this->returnValue($translator));

		$this->instance->origin = 'ORIGIN';

		$clientException = $this->getClassMock('GuzzleHttp\Exception\ClientException', ['getResponse']);
		$responseInterface = $this->getClassMock('GuzzleHttp\Psr7\Response', ['getBody']);
		$streamInterface = $this->getClassMock('GuzzleHttp\Psr7\Stream', ['getContents']);

		$streamInterface->expects($this->once())
			->method('getContents')
			->will($this->returnValue('{"error":{"code": 404,"details":[{"errorCode": "UNREGISTERED"}]}}'));

		$responseInterface->expects($this->once())
			->method('getBody')
			->will($this->returnValue($streamInterface));

		$clientException->expects($this->once())
			->method('getResponse')
			->will($this->returnValue($responseInterface));

		$this->instance->client = $this->getClassMock('GuzzleHttp\Client', ['request']);
		$this->instance->client->expects($this->once())
			->method('request')
			->will($this->throwException($clientException));

		$this->instance->sendByPlateAndVehicleType('AAA-1111', 'CARRO', 'TITLE', 'MESSAGE', 'TAG');

	}

	public function testSendByPlateAndVehicleTypeWithUnexpectedClientStatus() {

		$this->mock('ParkingNotifications\Notification\Requester', ['googleClient', 'translator']);

		$googleClient = $this->getClassMock('Google_Client', ['useApplicationDefaultCredentials', 'setScopes', 'refreshTokenWithAssertion', 'getAccessToken']);
		$googleClient->expects($this->once())
			->method('getAccessToken')
			->will($this->returnValue(['access_token' => 'AUTH_TOKEN']));
		$this->instance->expects($this->once())
			->method('googleClient')
			->will($this->returnValue($googleClient));

		$this->instance->connection = $this->getClassMock('PDO', ['prepare']);

		$stmt = $this->getClassMock('PDOStatement', ['bindValue', 'execute', 'fetchAll']);
		$stmt->expects($this->at(3))
			->method('fetchAll')
			->will($this->returnValue([
				[
					'entity_id' => 'ENTITY1_ID',
				]
			]));

		$stmt->expects($this->at(6))
			->method('fetchAll')
			->will($this->returnValue([
				[
					'token' => 'TOKEN1',
					'language' => 'LANGUAGE'
				]
			]));

		$this->instance->connection->expects($this->at(0))
			->method('prepare')
			->will($this->returnValue($stmt));

		$this->instance->connection->expects($this->at(1))
			->method('prepare')
			->will($this->returnValue($stmt));

		$translator = $this->getClassMock('ParkingNotifications\Notification\Translator', ['t']);
		$translator->expects($this->at(0))
			->method('t')
			->with('TITLE')
			->will($this->returnValue('TRANSLATED_TITLE'));
		$translator->expects($this->at(1))
			->method('t')
			->with('MESSAGE')
			->will($this->returnValue('TRANSLATED_MESSAGE'));

		$this->instance->expects($this->once())
			->method('translator')
			->with('LANGUAGE')
			->will($this->returnValue($translator));

		$this->instance->origin = 'ORIGIN';

		$clientException = $this->getClassMock('GuzzleHttp\Exception\ClientException', ['getResponse']);
		$responseInterface = $this->getClassMock('GuzzleHttp\Psr7\Response', ['getBody']);
		$streamInterface = $this->getClassMock('GuzzleHttp\Psr7\Stream', ['getContents']);

		$streamInterface->expects($this->once())
			->method('getContents')
			->will($this->returnValue('{"error":{"code": 400,"details":[{"errorCode": "UNREGISTERED"}]}}'));

		$responseInterface->expects($this->once())
			->method('getBody')
			->will($this->returnValue($streamInterface));

		$clientException->expects($this->once())
			->method('getResponse')
			->will($this->returnValue($responseInterface));

		$this->instance->client = $this->getClassMock('GuzzleHttp\Client', ['request']);
		$this->instance->client->expects($this->once())
			->method('request')
			->will($this->throwException($clientException));

		$this->instance->sendByPlateAndVehicleType('AAA-1111', 'CARRO', 'TITLE', 'MESSAGE', 'TAG');

	}

	public function testSendByPlateAndVehicleTypeWithUnexpectedClientExpection() {

		$this->mock('ParkingNotifications\Notification\Requester', ['googleClient', 'translator']);

		$googleClient = $this->getClassMock('Google_Client', ['useApplicationDefaultCredentials', 'setScopes', 'refreshTokenWithAssertion', 'getAccessToken']);
		$googleClient->expects($this->once())
			->method('getAccessToken')
			->will($this->returnValue(['access_token' => 'AUTH_TOKEN']));
		$this->instance->expects($this->once())
			->method('googleClient')
			->will($this->returnValue($googleClient));

		$this->instance->connection = $this->getClassMock('PDO', ['prepare']);

		$stmt = $this->getClassMock('PDOStatement', ['bindValue', 'execute', 'fetchAll']);
		$stmt->expects($this->at(3))
			->method('fetchAll')
			->will($this->returnValue([
				[
					'entity_id' => 'ENTITY1_ID',
				]
			]));

		$stmt->expects($this->at(6))
			->method('fetchAll')
			->will($this->returnValue([
				[
					'token' => 'TOKEN1',
					'language' => 'LANGUAGE'
				]
			]));

		$this->instance->connection->expects($this->at(0))
			->method('prepare')
			->will($this->returnValue($stmt));

		$this->instance->connection->expects($this->at(1))
			->method('prepare')
			->will($this->returnValue($stmt));

		$translator = $this->getClassMock('ParkingNotifications\Notification\Translator', ['t']);
		$translator->expects($this->at(0))
			->method('t')
			->with('TITLE')
			->will($this->returnValue('TRANSLATED_TITLE'));
		$translator->expects($this->at(1))
			->method('t')
			->with('MESSAGE')
			->will($this->returnValue('TRANSLATED_MESSAGE'));

		$this->instance->expects($this->once())
			->method('translator')
			->with('LANGUAGE')
			->will($this->returnValue($translator));

		$this->instance->origin = 'ORIGIN';

		$clientException = $this->getClassMock('GuzzleHttp\Exception\ClientException', ['getResponse']);
		$responseInterface = $this->getClassMock('GuzzleHttp\Psr7\Response', ['getBody']);
		$streamInterface = $this->getClassMock('GuzzleHttp\Psr7\Stream', ['getContents']);

		$streamInterface->expects($this->once())
			->method('getContents')
			->will($this->returnValue('{"error":{"code": 404,"details":[{"errorCode": "UNEXPECTED CODE"}]}}'));

		$responseInterface->expects($this->once())
			->method('getBody')
			->will($this->returnValue($streamInterface));

		$clientException->expects($this->once())
			->method('getResponse')
			->will($this->returnValue($responseInterface));

		$this->instance->client = $this->getClassMock('GuzzleHttp\Client', ['request']);
		$this->instance->client->expects($this->once())
			->method('request')
			->will($this->throwException($clientException));

		$this->instance->sendByPlateAndVehicleType('AAA-1111', 'CARRO', 'TITLE', 'MESSAGE', 'TAG');

	}

}

?>
