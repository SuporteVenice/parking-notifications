<?php

namespace ParkingNotificationsTests;

use PHPUnit\Framework\TestCase;

abstract class BaseTest extends TestCase {

	protected $instance;

	/**
	 * Método para obter o mock de uma classe
	 * @param  string $classPath    Namespace da classe
	 * @param  array  $methods 		Métodos que sofrerão o mock
	 */
	public function getClassMock($classPath, $methods = []) {

		return $this->getMockBuilder($classPath)
		 	->disableOriginalConstructor()
		 	->setMethods($methods)
		 	->getMock();

	}

	protected function mock($name, $methods = null) {
		
		$mock = $this->getClassMock($name, empty($methods)? null : $methods);

		$this->instance = $mock;

		return $mock;

	}

}

?>