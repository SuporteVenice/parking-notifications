<?php

use ParkingNotificationsTests\BaseTest;
use ParkingNotifications\Application;
use ParkingNotifications\Notification\Creator;
use ParkingNotifications\Parking\Checker;

class ApplicationTest extends BaseTest {

	public function testConstruct() {

		$this->mock('ParkingNotifications\Application', ['creator', 'checker']);

		$notificationCreator = $this->getClassMock('ParkingNotifications\Notification\Creator');

		$this->instance->expects($this->once())
			->method('creator')
			->with('CONNECTION', 'ORIGIN')
			->will($this->returnValue($notificationCreator));

		$parkingChecker = $this->getClassMock('ParkingNotifications\Parking\Checker');

		$this->instance->expects($this->once())
			->method('checker')
			->with('CONNECTION', $notificationCreator)
			->will($this->returnValue($parkingChecker));

		$this->instance->__construct('CONNECTION', 'ORIGIN');

		$this->assertEquals($notificationCreator, $this->instance->notificationCreator);
		$this->assertEquals($parkingChecker, $this->instance->parkingChecker);

	}

}

?>