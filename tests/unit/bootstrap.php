<?php 

// Define constantes obrigatórias para o projeto
define('ROOT', realpath(__DIR__ . "/../../"));

// Inclui lib do Composer para autoload de classes
require ROOT . "/vendor/autoload.php";

require ROOT . "/src/Constants.php";

?>