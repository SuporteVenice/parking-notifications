<?php

use ParkingNotificationsTests\BaseTest;
use ParkingNotifications\Parking\Checker;
use ParkingNotifications\Notification\Creator;

class CheckerTest extends BaseTest {

	public function testConstruct() {

		$this->mock('ParkingNotifications\Parking\Checker');

		$this->instance->__construct('CONNECTION', 'NOTIFICATION_CREATOR');

		$this->assertEquals('CONNECTION', $this->instance->connection);
		$this->assertEquals('NOTIFICATION_CREATOR', $this->instance->notificationCreator );

	}

	/**
	 * @expectedException \InvalidArgumentException
	 * @expectedExceptionMessage Numeric required
	 */
	public function testRemainingBalanceInvalidEntity() {

		$this->mock('ParkingNotifications\Parking\Checker');

		$this->instance->remainingBalance('NOT_NUMERIC_ENTITY');

	}

	public function testRemainingBalanceWithBalanceGreaterThanConfigurationValue() {

		$this->mock('ParkingNotifications\Parking\Checker');

		$this->instance->connection = $this->getClassMock('PDO', ['prepare']);

		$stmt1 = $this->getClassMock('PDOStatement', ['bindValue', 'execute', 'fetch']);
		$stmt1->expects($this->once())
			->method('bindValue')
			->with(1, 351);

		$stmt1->expects($this->once())
			->method('execute');
		$stmt1->expects($this->once())
			->method('fetch')
			->will($this->returnValue(['balance' => 10.00]));

		$query1 = "SELECT id, SUM(pre_creditado + pre_utilizado + pre_creditado_estorno + pre_utilizado_estorno) as balance
					FROM limite
					WHERE entidade_id = ?
					AND bolsa_id = 1
					LIMIT 1";
		$this->instance->connection->expects($this->at(0))
			->method('prepare')
			->with($query1)
			->will($this->returnValue($stmt1));

		$stmt2 = $this->getClassMock('PDOStatement', ['execute', 'fetch']);

		$stmt2->expects($this->once())
			->method('execute');
		$stmt2->expects($this->once())
			->method('fetch')
			->will($this->returnValue(['value' => 5.00]));

		$query2 = "SELECT valor as value
					FROM configuracao
					WHERE chave = 'AVISO_CLIENTE_SALDO_MINIMO'";
		$this->instance->connection->expects($this->at(1))
			->method('prepare')
			->with($query2)
			->will($this->returnValue($stmt2));

		$this->instance->notificationCreator = $this->getClassMock('ParkingNotifications\Notification\Creator', ['remainingBalance']);
		$this->instance->notificationCreator->expects($this->never())
			->method('remainingBalance');

		$this->instance->remainingBalance(351);

	}

	public function dataProviderTestRemainingBalanceWithBalanceLessThanOrEqualsConfigurationValue() {

		return [
			[5.00],
			[4.99],
			[0.00]
		];

	}

	/**
	 * @dataProvider dataProviderTestRemainingBalanceWithBalanceLessThanOrEqualsConfigurationValue
	 */
	public function testRemainingBalanceWithBalanceLessThanOrEqualsConfigurationValue($balance) {

		$this->mock('ParkingNotifications\Parking\Checker');

		$this->instance->connection = $this->getClassMock('PDO', ['prepare']);

		$stmt1 = $this->getClassMock('PDOStatement', ['bindValue', 'execute', 'fetch']);
		$stmt1->expects($this->once())
			->method('bindValue')
			->with(1, 351);

		$stmt1->expects($this->once())
			->method('execute');
		$stmt1->expects($this->once())
			->method('fetch')
			->will($this->returnValue(['balance' => $balance]));

		$query1 = "SELECT id, SUM(pre_creditado + pre_utilizado + pre_creditado_estorno + pre_utilizado_estorno) as balance
					FROM limite
					WHERE entidade_id = ?
					AND bolsa_id = 1
					LIMIT 1";
		$this->instance->connection->expects($this->at(0))
			->method('prepare')
			->with($query1)
			->will($this->returnValue($stmt1));

		$stmt2 = $this->getClassMock('PDOStatement', ['execute', 'fetch']);

		$stmt2->expects($this->once())
			->method('execute');
		$stmt2->expects($this->once())
			->method('fetch')
			->will($this->returnValue(['value' => 5.00]));

		$query2 = "SELECT valor as value
					FROM configuracao
					WHERE chave = 'AVISO_CLIENTE_SALDO_MINIMO'";
		$this->instance->connection->expects($this->at(1))
			->method('prepare')
			->with($query2)
			->will($this->returnValue($stmt2));

		$this->instance->notificationCreator = $this->getClassMock('ParkingNotifications\Notification\Creator', ['remainingBalance']);
		$this->instance->notificationCreator->expects($this->once())
			->method('remainingBalance');

		$this->instance->remainingBalance(351);

	}

	public function testRechargeSuccess() {

		$this->mock('ParkingNotifications\Parking\Checker');

		$this->instance->notificationCreator = $this->getClassMock('ParkingNotifications\Notification\Creator', ['rechargeSuccess']);
		$this->instance->notificationCreator->expects($this->once())
		    ->method('rechargeSuccess')
		    ->with(1234, '12,34');

		$this->instance->rechargeSuccess(1234, '12,34');
	}

	public function testRechargeSuccessWithIdAsNumericString() {

		$this->mock('ParkingNotifications\Parking\Checker');

		$this->instance->notificationCreator = $this->getClassMock('ParkingNotifications\Notification\Creator', ['rechargeSuccess']);
		$this->instance->notificationCreator->expects($this->once())
		    ->method('rechargeSuccess')
		    ->with(1234, '12,34');

		$this->instance->rechargeSuccess('1234', '12,34');
	}


	/**
	 * @expectedException \InvalidArgumentException
	 * @expectedExceptionMessage Numeric required
	 */
	public function testRechargeSuccessWithIdAsString() {

		$this->mock('ParkingNotifications\Parking\Checker');

		$this->instance->notificationCreator = $this->getClassMock('ParkingNotifications\Notification\Creator', ['rechargeSuccess']);
		$this->instance->notificationCreator->expects($this->never())
		    ->method('rechargeSuccess');

		$this->instance->rechargeSuccess('ENTITY_ID', '12,34');
	}

}

?>
