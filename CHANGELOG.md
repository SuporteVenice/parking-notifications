## PRÓXIMA RELEASE

## v1.10.0 (04/01/2019)

* [US4302](http://help.s2way.com:3000/issues/4302) Adicionada notificação de período excedente.

## v1.9.0 (18/12/2018)

* [US4301](http://help.s2way.com:3000/issues/4301) Adicionado tipo da irregularidade na notificação.

## v1.8.0 (10/08/2018)

### MELHORIAS:

* [US3999](http://help.s2way.com:3000/issues/3999) Incrementada versão do guzzlehttp para 6.3.3.

## v1.7.0 (26/06/2018)

### MELHORIAS:

* [RF3829](http://help.s2way.com:3000/issues/3829) Refatorada lógica no envio das notificações para validar se há dispositivos para receberem a notificação antes de buscar o token de autenticação para uso na requisição ao Firebase.

## v1.6.0 (25/06/2018)

### MELHORIAS:

* [US3818](http://help.s2way.com:3000/issues/3818) Implementada a configuração da URI utilizada pelo parking-notifications para o envio de notificações.

## v1.5.0 (19/06/2018)

### MELHORIAS:

* [US3746](http://help.s2way.com:3000/issues/3746) Adicionada função para envio de notificação de término de ticket.

## v1.4.1 (14/06/2018)

### CORREÇÕES DE BUGS:

* [BUG3785](http://help.s2way.com:3000/issues/3785) Correção no tratamento de exceção quando é realizada a busca pelo token de autenticação para utilização na api do firebase.

## v1.4.0 (22/05/2018)

### MELHORIAS:

* [US3466](http://help.s2way.com:3000/issues/3466) Adicionadas funções de envio de notificação para ativação, desativação e cancelamento de tickets.
